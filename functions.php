<?php

function noblogs_bp_about() {
  ?>
<div id="noblogs_about">
 
 <table border="0">
  <tr>
   <td class="apicetto_sx">&#8220;</td>

   <td class="about_text">
  ... una piattaforma di blogging
  <a href="/policy/">libera ed anonima</a>,

  a cura del collettivo
  <a href="http://www.autistici.org/">Autistici /
  Inventati</a>.
   </td>

   <td class="apicetto_dx">&#8222;</td>
  </tr>
 </table>

</div>
<?php
}


if ($_GET['test_home'] == '1') {
  add_action('bp_before_blog_home', 'noblogs_bp_about');
}


// override parent theme CSS loading

function bp_dtheme_enqueue_styles() {
    // enqueue the patched main stylesheet
    wp_enqueue_style( 'bp-default-main', get_stylesheet_directory_uri() . '/css/default.css', array(), bp_get_version() );

    // Register our child stylesheet
    wp_enqueue_style( get_stylesheet(), get_stylesheet_uri(), array( 'bp-default-main' ), bp_get_version() );

}
add_action( 'wp_enqueue_scripts', 'bp_dtheme_enqueue_styles' );
